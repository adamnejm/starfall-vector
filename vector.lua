
local XYZ = {
	x = 1, X = 1,
	y = 2, Y = 2,
	z = 3, Z = 3,
}

---@class Vector
---@field [any] any
---@operator mul(Vector|number):Vector
---@operator div(Vector|number):Vector
---@operator add(Vector):Vector
---@operator sub(Vector):Vector
---@operator unm:Vector
local methods = {}

local meta = {}

-------------------------------------------

--- Creates a new Vector object
---@return Vector vector
local function new(x, y, z)
	if not x then x = 0 end
	if not z then z = (y and 0 or x) end
	if not y then y = x end
	return setmetatable({ x, y, z }, meta)
end

local function wrap(tbl)
	return setmetatable(tbl, meta)
end


-------------------------------------------
--               METATABLE               --
-------------------------------------------

function meta.__index(self, k)
	local method = methods[k]
	if method ~= nil then
		return method
	elseif XYZ[k] then
		return rawget(self, XYZ[k])
	elseif type(k) == "string" then
		-- Swizzle support
		local v = { 0, 0, 0 }
		for i = 1, math.min(#k, 3) do
			local vk = XYZ[string.sub(k, i, i)]
			if vk then
				v[i] = rawget(self, vk)
			else
				return nil
			end
		end
		return wrap(v)
	end
end

function meta.__newindex(self, k, v)
	if XYZ[k] then
		rawset(self, XYZ[k], v)
	elseif type(k) == "string" then
		if string.len(k) == 2 and XYZ[string.sub(k, 1, 1)] and XYZ[string.sub(k, 2, 2)] then
			rawset(self, XYZ[string.sub(k, 1, 1)], rawget(v, 1) or 0)
			rawset(self, XYZ[string.sub(k, 2, 2)], rawget(v, 2) or 0)
		elseif string.len(k) == 3 and XYZ[string.sub(k, 1, 1)] and XYZ[string.sub(k, 2, 2)] and XYZ[string.sub(k, 3, 3)] then
			rawset(self, XYZ[string.sub(k, 1, 1)], rawget(v, 1) or 0)
			rawset(self, XYZ[string.sub(k, 2, 2)], rawget(v, 2) or 0)
			rawset(self, XYZ[string.sub(k, 3, 3)], rawget(v, 3) or 0)
		else
			rawset(self, k, v)
		end
	else
		rawset(self, k, v)
	end
end

function meta.__tostring(self)
	return string.format("[%s, %s, %s]", rawget(self, 1), rawget(self, 2), rawget(self, 3))
end


-------------------------------------------
--              METAEVENTS               --
-------------------------------------------


function meta.__mul(a, b)
	if type(b) == "number" then
		return wrap { a[1] * b, a[2] * b, a[3] * b }
	elseif type(a) == "number" then
		return wrap { b[1] * a, b[2] * a, b[3] * a }
	elseif getmetatable(a) == meta and getmetatable(b) == meta then
		return wrap { a[1] * b[1], a[2] * b[2], a[3] * b[3] }
	else
		error(string.format("Cannot multiply Vector by %s!", type(b)))
	end
end

function meta.__div(a, b)
	if type(b) == "number" then
		return wrap { a[1] / b, a[2] / b, a[3] / b }
	elseif type(a) == "number" then
		return wrap { b[1] / a, b[2] / a, b[3] / a }
	elseif getmetatable(a) == meta and getmetatable(b) == meta then
		return wrap { a[1] / b[1], a[2] / b[2], a[3] / b[3] }
	else
		error(string.format("Cannot divide Vector by %s!", type(b)))
	end
end

function meta.__add(a, b)
	assert(getmetatable(b) == meta, string.format("Cannot add %s to Vector!", type(b)))
	return wrap{ a[1] + b[1], a[2] + b[2], a[3] + b[3] }
end

function meta.__sub(a, b)
	assert(getmetatable(b) == meta, string.format("Cannot subtract %s from Vector!", type(b)))
	return wrap{ a[1] - b[1], a[2] - b[2], a[3] - b[3] }
end

function meta.__eq(a, b)
	return getmetatable(b) == meta and a[1] == b[1] and a[2] == b[2] and a[3] == b[3]
end

function meta.__unm(a)
	return wrap{ -a[1], -a[2], -a[3] }
end


-------------------------------------------
--                METHODS                --
-------------------------------------------

--- Returns a new Vector with the same X, Y and Z coordinates
---@return Vector clone
function methods:clone()
	return wrap { self[1], self[2], self[3] }
end

--- Checks whether the Vector is within a box defined by the 2 other Vectors
---@param mins Vector First corner of the box
---@param maxs Vector Second corner of the box
---@return boolean # True if Vector is within the box, False otherwise
function methods:is_within(mins, maxs)
	if self[1] < math.min(mins[1], maxs[1]) or self[1] > math.max(mins[1], maxs[1]) then return false end
	if self[2] < math.min(mins[2], maxs[2]) or self[2] > math.max(mins[2], maxs[2]) then return false end
	if self[3] < math.min(mins[3], maxs[3]) or self[3] > math.max(mins[3], maxs[3]) then return false end
	return true
end

-------------------------------------------

--- Returns cosine of the angle between both Vectors multiplied by their lengths
---@param v Vector
---@return number dot_product
function methods:get_dot(v)
	return (self[1] * v[1] + self[2] * v[2] + self[3] * v[3])
end
methods.dot = methods.get_dot

--- Creates a new perpendicular Vector based on self and the other Vector
---@param v Vector
---@return Vector cross_product
function methods:get_cross(v)
	return wrap { self[2] * v[3] - self[3] * v[2], self[3] * v[1] - self[1] * v[3], self[1] * v[2] - self[2] * v[1] }
end
methods.cross = methods.get_cross

--- Returns a new Vector with the same direction and length of 1
---@return Vector normalized_vector
function methods:get_normalized()
	local len = math.sqrt(self[1]^2 + self[2]^2 + self[3]^2)
	return wrap { self[1] / len, self[2] / len, self[3] / len }
end
methods.normalized = methods.get_normalized

--- Returns Vector's length
---@return number length
function methods:get_length()
	return math.sqrt(self[1]^2 + self[2]^2 + self[3]^2)
end
methods.length = methods.get_length

--- Returns Vector's length squared
---@return number length
function methods:get_length_sqr()
	return self[1]^2 + self[2]^2 + self[3]^2
end
methods.length_sqr = methods.get_length_sqr

--- Returns Pythagorean distance between self and the other Vector
---@param v Vector Other Vector
---@return number distance Distance
function methods:get_distance(v)
	return math.sqrt((v[1] - self[1])^2 + (v[2] - self[2])^2 + (v[3] - self[3])^2)
end
methods.distance = methods.get_distance

--- Returns Pythagorean distance squared between self and the other Vector
---@param v Vector Other Vector
---@return number distance Squared distance
function methods:get_distance_sqr(v)
	return (v[1] - self[1])^2 + (v[2] - self[2])^2 + (v[3] - self[3])^2
end
methods.distance_sqr = methods.get_distance_sqr

-------------------------------------------

--- **Self-modifying!**
--- Adds other Vector to self
---@return Vector self
function methods:add(v)
	self[1] = self[1] + v[1]
	self[2] = self[2] + v[2]
	self[3] = self[3] + v[3]
	return self
end

--- **Self-modifying!**
--- Subtracts other Vector from self
---@return Vector self
function methods:sub(v)
	self[1] = self[1] - v[1]
	self[2] = self[2] - v[2]
	self[3] = self[3] - v[3]
	return self
end

--- **Self-modifying!**
--- Multiplies self by the other Vector
---@return Vector self
function methods:mul(v)
	if getmetatable(v) == meta then
		self[1] = self[1] * v[1]
		self[2] = self[2] * v[2]
		self[3] = self[3] * v[3]
	else
		self[1] = self[1] * v
		self[2] = self[2] * v
		self[3] = self[3] * v
	end
	return self
end

--- **Self-modifying!**
--- Divides self by the other Vector
---@return Vector self
function methods:div(v)
	if getmetatable(v) == meta then
		self[1] = self[1] / v[1]
		self[2] = self[2] / v[2]
		self[3] = self[3] / v[3]
	else
		self[1] = self[1] / v
		self[2] = self[2] / v
		self[3] = self[3] / v
	end
	return self
end

--- **Self-modifying!**
--- Copies X, Y and Z coordinates from the given Vector
---@return Vector self
function methods:set(v)
	self[1] = v[1]
	self[2] = v[2]
	self[3] = v[3]
	return self
end

--- **Self-modifying!**
--- Set's X coordinate of self
---@param x number X coordinate
---@return Vector self
function methods:set_x(x)
	self[1] = x
	return self
end

--- **Self-modifying!**
--- Set's Y coordinate of self
---@param y number Y coordinate
---@return Vector self
function methods:set_y(y)
	self[2] = y
	return self
end

--- **Self-modifying!**
--- Set's Z coordinate of self
---@param z number Z coordinate
---@return Vector self
function methods:set_z(z)
	self[3] = z
	return self
end

--- **Self-modifying!**
--- Normalizes self to the length of 1 without changing the direction
---@return Vector self
function methods:normalize()
	local len = math.sqrt(self[1]^2 + self[2]^2 + self[3]^2)
	self[1] = self[1] / len
	self[2] = self[2] / len
	self[3] = self[3] / len
	return self
end

-------------------------------------------

return new
