
# **Vector.lua** - Vector library for Lua *>= 5.1*

This is a port of the excellent [Starfall's Vector library](https://github.com/thegrb93/StarfallEx/blob/master/lua/starfall/libs_sh/vectors.lua) to standard Lua.  
All credits go to [StarfallEx](https://github.com/thegrb93/StarfallEx/)'s maintainers and contributors.  

## Features

### Smart initialization
```lua
Vector()        --> Vector(0, 0, 0)
Vector(1)       --> Vector(1, 1, 1)
Vector(1, 2)    --> Vector(1, 2, 0)
Vector(1, 2, 3) --> Vector(1, 2, 3)
```

### Array coordinats
**Vector** coordinates are stored in an array, however it is still possible to get and set them using standard coordinate names:
```lua
local v = Vector(0, 0, 0)
v[1] = 1
v.y  = 2

print(v[1]) --> 1
print(v.y)  --> 2
```

### Swizzle support
You can get a brand new **Vector** object by querying existing **Vector** using any combination of `xyz` letters:
```lua
local v = Vector(1, 2, 3)
print(v.xz)  --> Vector(1, 3, 0)
print(v.xzy) --> Vector(1, 3, 2)
print(v.zzz) --> Vector(3, 3, 3)
```

It also works for self-modifying the **Vector**:
```lua
local v = Vector(1, 2, 3)
v.xz  = { 1, 2 }
print(v) --> 1, 2, 2
v.zyx = Vector(1, 2, 3)
print(v) --> 3, 2, 1
```

### Self-modifying functions
In addition to methods that produce a new **Vector** every time, there are equivalents that modify the **Vector** itself without creating a new object:
```lua
local v = Vector(5, 0, 0)
print(v:get_normalized(), v) --> Vector(1, 0, 0), Vector(5, 0, 0)
print(v:normalize(), v)      --> Vector(1, 0, 0), Vector(1, 0, 0)
```

### Omitting square root
For functions that make use of square root, there are more performant *squared* alternatives, eg. `get_distance` and `get_distance_sqr`

### Shorter function names
For clarity, functions that create a new **Vector** object are prefixed with `get_`, however the library offers aliases, eg. `cross` is an alias of `get_cross`.

## Documentation
Every function is documented using [sumneko's LSP](https://github.com/LuaLS/lua-language-server) annotation system.  
Meaning you can get dynamic syntax checking and autocompletion in [any code editor](https://microsoft.github.io/language-server-protocol/implementors/tools/) that supports the Language Server Protocol.


